package com;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

import com.Packet.Builder;

public class Client extends Thread{
	private ClientUi 		clientUi;
	
	private int 			dropRate;
	private DatagramSocket 	socket;
	
	private Sender 			packetSender;
	private Receiver 		packetReceiver;
	
	private boolean			connected;
	private	int				connectedPort;	
	
	private boolean			disconnectionReceived;
	
	public Client(ClientUi clientUi, int port, int receiveingWindowSize, int dropRate) throws SocketException {
		super();
		this.clientUi = clientUi;
		this.dropRate = dropRate;
		
		this.socket 		= new DatagramSocket(port);
		
		packetSender = new Sender();
		packetSender.start();
		
		packetReceiver = new Receiver(receiveingWindowSize);
		packetReceiver.start();
	}

	@Override
	public void run() {
		System.out.println("Listening at " + socket.getLocalAddress().getHostAddress() + ":" + socket.getLocalPort());
		
		while (true){
			byte[] buf = new byte[5000];
			DatagramPacket packet = new DatagramPacket(buf, buf.length);

			try {
				socket.receive(packet);
				packetReceiver.receivePacket(packet);
			} catch (IOException e) {
				e.printStackTrace();
				break;
			}
		}
	}
	
	private void sendPacket(Packet pckt){
		if (socket == null){
			clientUi.log("Error! null socket, restart application");
			return;
		}
		
		Random r = new Random();
		if (r.nextInt(100) + 1 > dropRate){
			
			try {
				socket.send(pckt.getDatagramPacket());
				clientUi.packetSent(pckt.toString());
				
			} catch (IOException e) {
				e.printStackTrace();
				clientUi.log("Error sending! " + e.getMessage());
			}
			
		}else clientUi.log("Packet dropped");
		
	}
	
	/* ===========================================================================================
	 * 								      CONNECT AND DISCONNECT
	 * ===========================================================================================
	 */
	public void connect(int serverPort) throws UnknownHostException {
		packetSender.sendPacket(Builder.getSYNPacket("localhost",serverPort));
	}
	
	public synchronized void disconnect(){
		packetSender.sendPacket(Builder.getDisconnectionPacket("localhost", connectedPort));
	}
	
	/* ===========================================================================================
	 * 								      CONNECTION ESTABLISHED
	 * ===========================================================================================
	 */	
	private void connected(String serverAddress, int serverPort, int sendingWindowSize){
		connected 		= true;	
		connectedPort 	= serverPort;
		
		clientUi.connected(serverAddress, serverPort);
		
		packetSender.setSendingWindow(sendingWindowSize);
		
		Random r = new Random();
		
		for(int i=0; i < r.nextInt(10) + 5; i++){
			
			//send random packets
			packetSender.sendPacket(Builder.getPayloadPacket("localhost", connectedPort, "Random data " + r.nextDouble()));
			
			try {
				Thread.sleep(500);
			} catch (InterruptedException e) {}
		}
	}
	
	private void disconnected(){
		this.connected		= false;
		this.connectedPort	= -1;
		
		clientUi.disconnected();
	}
	
	/* ===========================================================================================
	 * 								      	PACKET SENDER
	 * ===========================================================================================
	 */
	private class Sender extends Thread{
		private ArrayList<Packet> 	outGoing;
		private int 				sendingWindowSize;
		private int					SND_UNA, SND_NXT;
		
		private Timer 				networkWait;
		
		private	int					lastAck;
		private int 				nextSeqNum;
		
		private boolean 			timeout;
		private boolean				halfClosed;
		
		private Sender(){
			this.outGoing = new ArrayList<Packet>();
			Random r = new Random();
			nextSeqNum = r.nextInt(1000);
			reset();
		}
		
		private void reset(){
			lastAck = 0;
			timeout = false;		//if true, netwait should perform it sched
			if (networkWait != null)
				networkWait.cancel();
			
			networkWait = null;
			SND_UNA = 0;
			SND_NXT = 0;
			
			sendingWindowSize = 0;
			outGoing.clear();
		}
		
		public void sendPacket(Packet pckt){
			if (pckt.isAckBit() && pckt.getData().isEmpty()){
				//Acknowledgement packet, no timeout
				Client.this.sendPacket(pckt);
			}else{
				outGoing.add(pckt); //enqueue packet
			}
		}
		
		public synchronized void acknowledge(Packet tempPacket) {
			for(int i=0; i<outGoing.size(); i++){
				if (outGoing.get(i).getSequenceNumber() == (tempPacket.getAcknowledgementNumber() - 1)){
					
					if (outGoing.get(i).isFinBit()){
						if (disconnectionReceived){
							reset();
							Client.this.disconnected();
						}else{
							halfClosed = true; //ELSE half closed connection, still listening but stopped sending
						}
					}
					
					lastAck = tempPacket.getAcknowledgementNumber();
					
					SND_UNA = i + 1;
					setSendingWindow(tempPacket.getWindowSize());
					
					timeout = false;
					
					if (networkWait != null)
						networkWait.cancel();
					
					return;
				}
			}
		}

		public synchronized void setSendingWindow(int sendingWindowSize) {
			this.sendingWindowSize = sendingWindowSize; 
		}

		private boolean continueSending;
		@Override
		public void run() {
			continueSending = true;
			while(isContinueSend()){
				
				if (timeout || outGoing.size() < 1){
					if (halfClosed){
						reset();
						Client.this.disconnected();
					}
					return;
				}
				
				if (Client.this.connected){
					
					if (outGoing.get(SND_NXT).isSynchBit()){
						//error SYN for 3-way handshake only
						outGoing.remove(SND_NXT);
						return;
					}
					
					if (outGoing.get(SND_NXT).getData().getBytes().length + consumedSendWindow() < sendingWindowSize){
						return;
					}
					
					Packet temp = outGoing.get(SND_NXT++);
					nextSeqNum = temp.setSending(nextSeqNum, lastAck, packetReceiver.getAvailableWindowSize());
					Client.this.sendPacket(temp);
					
				}else{
					Packet temp = outGoing.get(SND_NXT++);
					nextSeqNum = temp.setSending(nextSeqNum, lastAck, packetReceiver.getAvailableWindowSize());
					Client.this.sendPacket(temp);
					
					//Dont wait for ack if ack is sent
					if (temp.isAckBit() && !temp.isSynchBit())
						return;
				}
				
				
				//wait for acknowledgement
				timeout = true;
				networkWait = new Timer();
				networkWait.schedule(new TimerTask(){
					@Override
					public void run() {
						if (timeout){
							SND_NXT = SND_UNA;								
							timeout = false;
						}
					}
				}, 4000);
			}
		}
		
		private int consumedSendWindow(){
			int sum = 0;
			for(int i=SND_UNA; i < outGoing.size() || i < SND_NXT; i++){
				sum += outGoing.get(i).getData().getBytes().length;
			}
			return sum;
		}
		
		private synchronized boolean isContinueSend(){
			return continueSending;
		}
		
		public synchronized void stopSending(){
			continueSending = false;
		}
	}
	
	/* ===========================================================================================
	 * 								      	PACKET RECEIVER
	 * ===========================================================================================
	 */
	private class Receiver extends Thread{
		private int					receiveingWindowSize;
		private int 				availableWindowSize;
		
		private boolean				bufferFull;		
		private ArrayDeque<String>	buffer;
		
		public Receiver(int receiveingWindowSize) {
			this.buffer 						= new ArrayDeque<String>();
			
			this.receiveingWindowSize = receiveingWindowSize;
			this.availableWindowSize = receiveingWindowSize;
		}
		
		public int getAvailableWindowSize() {
			return availableWindowSize;
		}

		public void receivePacket(DatagramPacket pckt){
			Packet tempPacket = Packet.Builder.getPacket(pckt);
			
			clientUi.packetReceived(tempPacket.toString());
			if (!connected){
				//3-way handshake
				if (tempPacket.isSynchBit() && !tempPacket.isAckBit()){
					//SYN
					sendPacket(Builder.getSYNACKPacket(pckt, tempPacket.getSequenceNumber(), tempPacket.getData().getBytes().length));
					
				}else if (tempPacket.isSynchBit() && tempPacket.isAckBit()){
					//SYN-ACK
					sendPacket(Builder.getAckPacket(pckt, tempPacket.getSequenceNumber(), tempPacket.getData().getBytes().length));
					connected(pckt.getAddress().getHostAddress(), pckt.getPort(), tempPacket.getWindowSize());
				}else if (!tempPacket.isSynchBit() && tempPacket.isAckBit()){
					//ACK
					connected(pckt.getAddress().getHostAddress(), pckt.getPort(), tempPacket.getWindowSize());
				}
			}else{
				if (pckt.getPort() != connectedPort) return;
				
				if (tempPacket.isSynchBit()){
					//Error SYN only used in handshake
					System.out.println("Error!: SYN only used in handshake");
					
				}else if (tempPacket.isFinBit()){
					packetSender.sendPacket(Builder.getFinAckPacket(pckt, tempPacket.getSequenceNumber(), tempPacket.getData().getBytes().length));
					disconnectionReceived = true;
					
				}else{
					if (!tempPacket.getData().isEmpty()){
						
						if (tempPacket.getData().getBytes().length < availableWindowSize){
							buffer.add(tempPacket.getData());
							availableWindowSize -= tempPacket.getData().getBytes().length;
							
							packetSender.sendPacket(Builder.getAckPacket(pckt, tempPacket.getSequenceNumber(), tempPacket.getData().getBytes().length));
						}else{
							availableWindowSize = 0;
							
							packetSender.sendPacket(Builder.getAckPacket(pckt));
						}
					}else{
						if (tempPacket.isAckBit()) packetSender.acknowledge(tempPacket);
					}
				}
			}
		}
				
		private boolean continueReceiving;
		@Override
		public void run() {
			continueReceiving = true;
			while(isContinueReceiving()){
				Iterator<String> i = buffer.iterator();
				while(i.hasNext()){
					String temp = i.next();
					
					clientUi.packetReceived("Data received: " + temp);
					availableWindowSize += temp.getBytes().length;
					
					i.remove();
				}
				
				if (bufferFull){
					//send ack with full windowsize
					availableWindowSize = receiveingWindowSize;
					packetSender.sendPacket(Builder.getAckPacket("localhost", connectedPort));
				}
				
				//2-Second delay
				try {
					Thread.sleep(2000);
				} catch (InterruptedException e) {}
			}
		}
		
		private synchronized boolean isContinueReceiving(){
			return continueReceiving;
		}
		
		public synchronized void stopReceiving(){
			continueReceiving = false;
		}
	}
}
