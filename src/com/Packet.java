package com;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class Packet {
	private DatagramPacket 	datagramPacket;
	
	private boolean		ackSet;
	
	private int 		sequenceNumber;
	private int 		acknowledgementNumber;
	private boolean 	synchBit;
	private boolean 	ackBit;
	private boolean 	finBit;
	private int 		windowSize;
	private String		data;
	
	private InetAddress receiverAddress;
	private int 		receiverPort;
	
	private Packet(	InetAddress receiverAddress, int receiverPort, 
					int sequenceNumber, String data, 
					int acknowledgementNumber, 
					boolean synchBit, boolean ackBit, boolean finBit) {
		super();
		this.receiverAddress 	= receiverAddress;
		this.receiverPort 		= receiverPort;
		
		this.sequenceNumber = sequenceNumber;
		this.acknowledgementNumber = acknowledgementNumber;
		this.synchBit = synchBit;
		this.ackBit = ackBit;
		this.finBit = finBit;
		this.data = data;
	}

	private Packet(DatagramPacket pckt, String[] parsed){
		this.datagramPacket 		= pckt;
		this.sequenceNumber 		= Integer.valueOf(parsed[0]);
		this.acknowledgementNumber 	= Integer.valueOf(parsed[1]);
		this.synchBit 				= Boolean.valueOf(parsed[2]);
		this.ackBit 				= Boolean.valueOf(parsed[3]);
		this.finBit 				= Boolean.valueOf(parsed[4]);
		this.windowSize 			= Integer.valueOf(parsed[5]);
		this.data 					= (parsed.length < 7)? "":parsed[6];
	}
	
	public DatagramPacket getDatagramPacket() {
		if (this.datagramPacket == null)
			this.datagramPacket = new DatagramPacket(this.toString().getBytes(), 
				this.toString().getBytes().length, receiverAddress, receiverPort);
		
		return datagramPacket;
	}

	public int getSequenceNumber() {
		return sequenceNumber;
	}

	public int getAcknowledgementNumber() {
		return acknowledgementNumber;
	}

	public boolean isSynchBit() {
		return synchBit;
	}

	public boolean isAckBit() {
		return ackBit;
	}

	public boolean isFinBit() {
		return finBit;
	}

	public int getWindowSize() {
		return windowSize;
	}

	public String getData() {
		return data;
	}
	
	public void setAckSet(){
		ackSet = true;
	}
	
	public int setSending(int nextSeqNum, int lastAck, int availableWindowSize) {
		this.sequenceNumber = nextSeqNum;
		this.acknowledgementNumber = (ackSet)? this.acknowledgementNumber:lastAck;
		this.windowSize = availableWindowSize;
		
		return sequenceNumber + data.getBytes().length + 1 ;
	}

	@Override
	public String toString() {
		return 	String.valueOf(sequenceNumber) + "/" +
				String.valueOf(acknowledgementNumber) + "/" +
				String.valueOf(synchBit) + "/" +
				String.valueOf(ackBit) + "/" +
				String.valueOf(finBit) + "/" + 
				String.valueOf(windowSize) + "/" + 
				data;
	}
	
	public String displayContent(){
		return 	"SEQNum: " + String.valueOf(sequenceNumber) + " " +
				"ACKNum: " + String.valueOf(acknowledgementNumber) + " " +
				"SYN: " + String.valueOf(synchBit) + " " +
				"ACK: " + String.valueOf(ackBit) + " " +
				"FIN: " + String.valueOf(finBit) + " " + 
				"WINSize: " + String.valueOf(windowSize) + " " + 
				"Data: " + data;
	}

	
	
	/* ===========================================================================================
	 * 								      PACKET BUILDER
	 * ===========================================================================================
	 */

	public static final class Builder{
		/**
		 * -------------------------- READ DATAGRAM PACKET --------------------------
		 * @param pckt
		 * @return
		 */
		public static final Packet getPacket(DatagramPacket pckt){
			Packet temp = null;
			
			String received = new String(pckt.getData(), 0, pckt.getLength());
			
			String[] parsed = received.split("/");
			if (parsed.length < 6){
				System.out.println("Error parsing Action data packet result with length: " + parsed.length);
			}else{
				temp = new Packet(pckt, parsed);
			}
			
			return temp;
		}
		
		public static Packet getSYNPacket(String receiverAddress, int receiverPort) throws UnknownHostException{
			Packet temp = new Packet(InetAddress.getByName(receiverAddress), receiverPort, 0, "", 0, 
					true, false, false);
			return temp;
		}
		
		public static Packet getDisconnectionPacket(String address, int port) {
			Packet temp = null;
			
			try {
				temp = new Packet(InetAddress.getByName(address), port, 0, "", 0, 
						false, false, true);
			} catch (UnknownHostException e) {}
				
			return temp;
		}

		public static Packet getPayloadPacket(String address, int port, String data) {
			Packet temp = null;
			
			try {
				temp = new Packet(InetAddress.getByName(address), port, 0, data, 0, 
						false, false, false);
			} catch (UnknownHostException e) {}
				
			return temp;
		}

		public static Packet getSYNACKPacket(DatagramPacket packet, int sequenceNumber, int dataLength){
			Packet temp = new Packet(packet.getAddress(), packet.getPort(), 0, "", sequenceNumber + dataLength + 1,  
					true, true, false);
			return temp;
		}	
		
		public static Packet getAckPacket(String address, int port) {
			Packet temp = null;
			
			try {
				temp = new Packet(InetAddress.getByName(address), port, 0, "", 0, 
						false, true, false);
			} catch (UnknownHostException e) {}
				
			return temp;
		}
		
		public static Packet getAckPacket(DatagramPacket pckt) {
			Packet temp = new Packet(pckt.getAddress(), pckt.getPort(), 0, "", 0, 
					false, true, false);
				
			return temp;
		}

		public static Packet getFinAckPacket(DatagramPacket pckt, int sequenceNumber, int dataLength) {
			Packet temp = new Packet(pckt.getAddress(), pckt.getPort(), 0, "", sequenceNumber + dataLength + 1, 
					false, true, true);
			temp.setAckSet();
				
			return temp;
		}

		public static Packet getAckPacket(DatagramPacket pckt, int sequenceNumber, int dataLength) {
			Packet temp = new Packet(pckt.getAddress(), pckt.getPort(), 0, "", sequenceNumber + dataLength + 1, 
					false, true, false);
			temp.setAckSet();
				
			return temp;
		}
	}
}
