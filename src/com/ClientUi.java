package com;

import java.awt.Font;
import java.awt.List;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.Random;

import javax.swing.DefaultComboBoxModel;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;

import net.miginfocom.swing.MigLayout;

public class ClientUi implements ActionListener{
	public static void main(String[] args){
		new ClientUi();
	}
	
	private JFrame frmClient;
	private JTextField txtWindowSize;
	private JButton btnStart;
	
	private List lstPacketsReceived;
	private List lstPacketsSent;

	private Client client;
	
	private JButton btnDisconnect;
	private JComboBox<String> cmbDropRate;
	private JLabel lblPacketDropRate;
	private JLabel lblTimerCaption;
	private JLabel lblTimer;
	private JLabel lblOnlineAtPort;
	private JLabel lblPort;
	private JLabel lblPortCaption;
	private JTextField txtPort;
	private JLabel lblConnectedTo;
	private JLabel lblRemoteAddress;
	/**
	 * Create the application.
	 */
	public ClientUi() {
		initialize();
		frmClient.setVisible(true);
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmClient = new JFrame();
		frmClient.setTitle("Device");
		frmClient.setResizable(false);
		frmClient.setBounds(100, 100, 784, 434);
		frmClient.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmClient.getContentPane().setLayout(new MigLayout("", "[14.00][76.00,grow][10.00][102.00][12.00][153.00,grow][13.00]", "[][][][][][][][][][grow][][][12.00][][][]"));
		
		JLabel lblNewLabel = new JLabel("Received");
		lblNewLabel.setFont(new Font("Tahoma", Font.PLAIN, 18));
		frmClient.getContentPane().add(lblNewLabel, "cell 1 1,alignx center");
		
		lblOnlineAtPort = new JLabel("Online at port");
		frmClient.getContentPane().add(lblOnlineAtPort, "cell 3 1,alignx center");
		
		JLabel lblPacketsSent = new JLabel("Sent");
		lblPacketsSent.setFont(new Font("Tahoma", Font.PLAIN, 18));
		frmClient.getContentPane().add(lblPacketsSent, "cell 5 1,alignx center,aligny baseline");
		
		lblPort = new JLabel("5000");
		frmClient.getContentPane().add(lblPort, "cell 3 2,alignx center");
		
		lstPacketsReceived = new List();
		frmClient.getContentPane().add(lstPacketsReceived, "cell 1 3 1 9,grow");
		
		lstPacketsSent = new List();
		frmClient.getContentPane().add(lstPacketsSent, "cell 5 3 1 9,grow");
		
		btnStart = new JButton("Start");
		btnStart.addActionListener(this);
		
		lblConnectedTo = new JLabel("Connected to");
		frmClient.getContentPane().add(lblConnectedTo, "cell 3 4,alignx center");
		
		lblRemoteAddress = new JLabel("remote address");
		frmClient.getContentPane().add(lblRemoteAddress, "cell 3 5,alignx center");
		
		lblTimerCaption = new JLabel("Timer");
		frmClient.getContentPane().add(lblTimerCaption, "cell 3 7,alignx center");
		
		lblTimer = new JLabel("00:00");
		frmClient.getContentPane().add(lblTimer, "cell 3 8,alignx center");
		frmClient.getContentPane().add(btnStart, "cell 3 10,grow");
		
		btnDisconnect = new JButton("Stop sending");
		btnDisconnect.addActionListener(this);
		btnDisconnect.setEnabled(false);
		frmClient.getContentPane().add(btnDisconnect, "cell 3 11,grow");
		
		lblPortCaption = new JLabel("My port");
		frmClient.getContentPane().add(lblPortCaption, "cell 1 13,alignx center");
		
		JLabel lblServerPort = new JLabel("Window size");
		frmClient.getContentPane().add(lblServerPort, "cell 3 13,alignx center,aligny top");
		
		lblPacketDropRate = new JLabel("Packet drop rate");
		frmClient.getContentPane().add(lblPacketDropRate, "cell 5 13,alignx center");
		
		txtPort = new JTextField();
		txtPort.setColumns(10);
		frmClient.getContentPane().add(txtPort, "cell 1 14,growx");
		
		txtWindowSize = new JTextField();
		frmClient.getContentPane().add(txtWindowSize, "cell 2 14 3 1,growx");
		txtWindowSize.setColumns(10);
		
		cmbDropRate = new JComboBox<String>();
		cmbDropRate.setModel(new DefaultComboBoxModel<String>(new String[] {"0%", "25%", "50%", "75%"}));
		cmbDropRate.setSelectedIndex(0);
		frmClient.getContentPane().add(cmbDropRate, "cell 5 14,growx");
		
		lblOnlineAtPort.setVisible(false);
		lblPort.setVisible(false);
		
		lblConnectedTo.setVisible(false);
		lblRemoteAddress.setVisible(false);
		
		lblTimerCaption.setVisible(false);
		lblTimer.setVisible(false);
		
		Random r = new Random();
		txtPort.setText(String.valueOf(1024 + r.nextInt(3000) + 1));
		txtWindowSize.setText(String.valueOf(3000));
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		if (e.getSource().equals(btnStart)){
			if (btnStart.getText().equalsIgnoreCase("Start")){
				/**
				 * ------------ START LISTENING TO SPECIFIED PORT ---------
				 */
				try {
					if (txtPort.getText().trim().isEmpty() || txtWindowSize.getText().trim().isEmpty()){
						showPrompt("Empty required fields!");
						return;
					}
					
					client = new Client(	this,
											Integer.valueOf(txtPort.getText().trim()),
											Integer.valueOf(txtWindowSize.getText().trim()),
											Integer.valueOf(cmbDropRate.getSelectedItem().toString().replace('%', ' ').trim()));
					client.start();
					
					lblPort.setText(txtPort.getText().trim());
					lblOnlineAtPort.setVisible(true);
					lblPort.setVisible(true);
					
					btnStart.setText("Connect to remote");
					btnDisconnect.setEnabled(false);
					txtWindowSize.setEditable(false);
					
					lblPortCaption.setText("Remote port");
					txtPort.setText("");
					
					
				} catch (NumberFormatException e1) {
					showPrompt("NaN Error: " + e1.getMessage());
				} catch (SocketException e1) {
					showPrompt(e1.getMessage());
				}
				
			}else{
				
				/**
				 * ----------------- CONNECT TO SERVER -----------------
				 */
				
				try {
					client.connect(Integer.valueOf(txtPort.getText().trim()));
					btnStart.setEnabled(false);
					txtPort.setEditable(false);
				} catch (NumberFormatException e1) {
					showPrompt("NaN Error: " + e1.getMessage());
				} catch (UnknownHostException e1) {
					showPrompt(e1.getMessage());
				}				
			}
			
		}else if(e.getSource().equals(btnDisconnect)){
			
			/**
			 * --------------- DISCONNECT FROM SERVER ---------------
			 */
			client.disconnect();
		}
	}
	
	public void connected(String ipAddress, int port){
		btnDisconnect.setEnabled(true);
		
		lblRemoteAddress.setText(ipAddress + ":" + port);
		lblConnectedTo.setVisible(true);
		lblRemoteAddress.setVisible(true);
	}

	public void disconnected(){
		btnStart.setEnabled(true);
		btnDisconnect.setEnabled(false);
		
		txtPort.setEditable(true);
	}
	
	
	public void packetReceived(String packet){
		lstPacketsReceived.add(packet);
	}
	
	public void packetSent(String packet){
		lstPacketsSent.add(packet);
	}
	
	public void log(String log){
		lstPacketsSent.add(log);
	}
	
	public void showPrompt(String message){
		JOptionPane.showMessageDialog(frmClient, message);
	}
}
